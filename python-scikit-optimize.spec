%global pypi_name scikit_optimize

Name:           python-scikit-optimize
Version:        0.10.2
Release:        1
Summary:        Sequential model-based optimization toolbox
License:        BSD-3-Clause
URL:            https://github.com/holgern/scikit-optimize
Source0:        https://files.pythonhosted.org/packages/source/s/%{pypi_name}/%{pypi_name}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python3-devel
Requires:       python3-numpy
Requires:       python3-scikit-learn >= 0.19.1
Requires:       python3-scipy >= 0.14.0

%global _description \
Scikit-Optimize, or skopt, is a simple and efficient library to \
minimize (very) expensive and noisy black-box functions. It implements \
several methods for sequential model-based optimization. skopt aims to \
be accessible and easy to use in many contexts.

%description %{_description}

%package -n     python3-scikit-optimize
Summary:        %{summary}
%{?python_provide:%python_provide python3-scikit-optimize}

%description -n python3-scikit-optimize %{_description}

%prep
%autosetup -n %{pypi_name}-%{version} -p1

%build
%py3_build

%install
%py3_install

%check

%files -n python3-scikit-optimize
%license LICENSE
%doc README.rst
%{python3_sitelib}/*

%changelog
* Tue Aug 13 2024 yaoxin <yao_xin001@hoperun.cpm> - 0.10.2-1
- Update to 0.10.2:
  * Update Pandas import to new format
  * Fix Typo in skopt/space/space.py
  * Add support for multimetric scoring to :obj:skopt.searchcv.BayesSearchCV
  * Fix Keep order of variables in LabelEncoder
  * Replace occurrences of mse with squared_error
  * Fix tuple index out of range on plot
  * Fixes Grid sampling with border="only"
  * Fix plot_gaussian_process not working with ps-acquisition

* Thu Nov 10 2022 yaoxin <yaoxin30@h-partners.com> - 0.9.0-2
- Change Source

* Thu Aug 04 2022 liukuo <liukuo@kylinos.cn> - 0.9.0-1
- upgrade version to 0.9.0

* Thu Jan 28 2021 liudabo<liudabo1@huawei.com> - 0.8.1-1
- upgrade version to 0.8.1

* Thu Dec 10 2020 shixuantong<shixuantong@huawei.com> - 0.7.4-2
- update source0

* Fri Jul 31 2020 Zhipeng Xie<xiezhipeng1@huawei.com> - 0.7.4-1
- upgrade to 0.7.4

* Thu Nov 7 2019 caomeng <caomeng5@huawei.com> - 0.5.2-1
- Init package
